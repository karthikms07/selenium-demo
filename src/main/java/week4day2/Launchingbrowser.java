package week4day2;

import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Launchingbrowser {
 
	public void allwindows() {
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
		//drowndownby using SELECTBYVISIBLETEXT
		
		WebElement WB = driver.findElementById("");
		Select sc = new Select(WB);
		sc.deselectByVisibleText("");
		
		//Dropdown using selectbyvalue
		
		sc.selectByValue("");
		//Dropdown using selectbyindex
		sc.selectByIndex(0);
	
		//list count of options tag
		List<WebElement> alloptions = sc.getOptions();
		int count = alloptions.size();
		
		//All verification
		 String text = driver.findElementById("").getText();
		 
		 //To Get the URL 
		 
		// String currentUrl = driver.getCurrentUrl();
		 
		 String currentUrl = driver.getCurrentUrl();
		 
		 //Toget the Title
		 
		 String title = driver.getTitle();
		 
		 boolean selected = driver.findElementById("").isSelected();
		 
		 //switch to window
		 
		 Set<String> AllwindowHandles = driver.getWindowHandles();
		 
		 
		 //To take screenshots
		 File src = driver.getScreenshotAs(OutputType.FILE);
		 File des= new File("");
		 
		 
		 
		 //SwitchtoAlert
		 Alert alert = driver.switchTo().alert();
		 alert.accept();
		 alert.dismiss();
		 alert.sendKeys("");
		 alert.getText();
		// driver.switchTo().alert().dismiss();
		 //driver.switchTo().alert().sendKeys("");
		 
		 
		 //Switch to frames
		 
		 driver.switchTo().frame(0);
		 driver.switchTo().frame(0);
		 driver.switchTo().frame(driver.findElementById(""));
		 driver.switchTo().frame("id or name");
		 driver.switchTo().defaultContent();
		 driver.switchTo().parentFrame();
		 
		 //webtable -> 4th row -> 3 rd column ---> read text
		 
		 
	WebElement Element = driver.findElementById("");
	List<WebElement> rows = Element.findElements(By.tagName("tr"));
	 List<WebElement> columns = rows.get(3).findElements(By.tagName("td"));
	 System.out.println(columns.get(2).getText());
	 
	 
	 //waits
	 driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	 
	 
	 //locators
	 
	 driver.findElementById("").clear();
	 driver.findElementById("").sendKeys();
	 driver.findElementByXPath("").click();
	 driver.findElementsByClassName("").
	 driver.findElementByLinkText("").getText();
	 
	 
	 
	 
	
	
	
	
		 
		 
		 
		 
		 
	      
		
		
		
		
		
		

	}

}
